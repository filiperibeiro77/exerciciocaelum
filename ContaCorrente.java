public class ContaCorrente extends Conta{

    public ContaCorrente(){
    
    }

    public ContaCorrente(double umSaldo) {
        super(umSaldo);
    }
    
     public double atualiza(double taxa) {
	saldo = saldo + (saldo * taxa * 2);
        return saldo;
    }
    
    public double deposita(double umValor){
	saldo = saldo + umValor - 0.10;
        return saldo;
    } 
     
}

