public class Conta {
    protected double saldo;

    public Conta(){
    
    }
    
    public Conta(double umSaldo){
        this.saldo = umSaldo;
    }
    
    public double getSaldo(){
        return saldo;
    }
    
    public void setSaldo(double umSaldo){
        this.saldo = umSaldo;
    }
    
    public double deposita(double umValor){
	saldo = saldo + umValor;
        return saldo;
    }
    
     public double saca(double umValor){
	saldo = saldo - umValor;
        return saldo;
     }
    
    public double atualiza(double taxa) {
	saldo = saldo + saldo * taxa;
        return saldo;
    }
 
     
}

