import java.io.*;

public class TesteContas {
    public static void main(String[] args){
        Conta umaConta = new Conta();
        ContaCorrente umaContaCorrente = new ContaCorrente();
        ContaPoupanca umaContaPoupanca = new ContaPoupanca();
        
        umaConta.deposita(5);
        umaConta.atualiza(0.01);

        umaContaCorrente.deposita(1000);
        umaContaCorrente.atualiza(0.01);
        
        umaContaPoupanca.deposita(1000);
        umaContaPoupanca.atualiza(0.01);
        
        System.out.println(umaConta.getSaldo());
        System.out.println(umaContaCorrente.getSaldo());
        System.out.println(umaContaPoupanca.getSaldo());
        

    }
}

